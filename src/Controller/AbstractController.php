<?php

namespace App\Controller\v1;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class AbstractController extends AbstractFOSRestController {
    /**
     * @throws UnprocessableEntityHttpException
     */
    protected function handleViolations(ConstraintViolationListInterface $violations): void
    {
        $errors = $this->processViolations($violations);

        $this->actOnViolations($errors);
    }

    /**
     * @return array<string, string>
     */
    protected function processViolations(ConstraintViolationListInterface $violations): array
    {
        if (!count($violations)) {
            return [];
        }

        $errors = [];

        foreach($violations as $violation) {
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $errors;
    }

    /**
     * @param array<string, string> $errors
     * 
     * @throws UnprocessableEntityHttpException
     */
    protected function actOnViolations(array $errors): void
    {
        if ($errors) {
            throw new UnprocessableEntityHttpException(json_encode($errors));
        }
    }
}