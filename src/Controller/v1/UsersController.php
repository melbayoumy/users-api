<?php

namespace App\Controller\v1;

use App\Controller\v1\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Doctrine\ORM\EntityManagerInterface;

#[Route('/users')]
/**
 * @OA\Tag(name="users")
 */
class UsersController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private UserRepository $userRepo
    )
    {
    }

    #[Route(['path' => '/', 'methods' => 'GET'])]
    /**
     * @OA\Get(summary="Get all users")
     * 
     * @OA\Response(
     *     response=200,
     *     description="All users",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=User::class))
     *     )
     * )
     */
    public function getAll(): View
    {
        $users = $this->userRepo->findAll();
        return $this->view($users);
    }

    #[Route(['path' => '/{id}', 'methods' => 'GET', 'requirements' => ['id' => '\d+']])]
    /**
     * @ParamConverter("user")
     * @OA\Get(summary="Get user by id")
     * 
     * @OA\Response(
     *     response=200,
     *     description="User",
     *     @Model(type=User::class)
     * )
     * 
     * @OA\Response(
     *     response=404,
     *     ref="#/components/responses/404"
     * )
     */
    public function getById(User $user): View
    {
        return $this->view($user);
    }

    #[Route(['path' => '/', 'methods' => 'POST'])]
    /**
     * @ParamConverter("user", converter="fos_rest.request_body")
     * @OA\Post(summary="Create new user")
     * 
     * @OA\RequestBody(
     *      required=true,
     *      @Model(type=User::class, groups={"in_out"})
     * )
     * 
     * @OA\Response(
     *     response=201,
     *     description="User created successfully",
     *     @Model(type=User::class)
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     ref="#/components/responses/400"
     * )
     * 
     * @OA\Response(
     *     response=415,
     *     ref="#/components/responses/415"
     * )
     * 
     * @OA\Response(
     *     response=422,
     *     ref="#/components/responses/422"
     * )
     */
    public function create(User $user, ConstraintViolationListInterface $violations): View
    {
        $this->handleViolations($violations);

        $this->em->persist($user);
        $this->em->flush();

        return $this->view($user, Response::HTTP_CREATED);
    }

    #[Route(['path' => '/{id}', 'methods' => 'PUT', 'requirements' => ['id' => '\d+']])]
    /**
     * @ParamConverter("user")
     * @ParamConverter("userNew", converter="fos_rest.request_body")
     * @OA\Put(summary="Replace user by id")
     * 
     * @OA\RequestBody(
     *      required=true,
     *      @Model(type=User::class, groups={"in_out"})
     * )
     * 
     * @OA\Response(
     *     response=200,
     *     description="User updated successfully",
     *     @Model(type=User::class)
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     ref="#/components/responses/400"
     * )
     *
     * @OA\Response(
     *     response=404,
     *     ref="#/components/responses/404"
     * )
     *
     * @OA\Response(
     *     response=415,
     *     ref="#/components/responses/415"
     * )
     * 
     * @OA\Response(
     *     response=422,
     *     ref="#/components/responses/422"
     * )
     */
    public function replaceById(User $user, User $userNew, ConstraintViolationListInterface $violations): View
    {
        // Bypass email violation if it belongs to the same user
        $errors = $this->processViolations($violations);

        if ($errors['email'] && $user->getEmail() == $userNew->getEmail()) {
            unset($errors['email']);
        }

        $this->actOnViolations($errors);
        //

        $user->setEmail($userNew->getEmail());
        $user->setFirstname($userNew->getFirstname());
        $user->setSurname($userNew->getSurname());

        $this->em->flush();

        return $this->view($user);
    }
    
    #[Route(['path' => '/{id}', 'methods' => 'DELETE', 'requirements' => ['id' => '\d+']])]
    /**
     * @ParamConverter("user")
     * @OA\Delete(summary="Delete user by id")
     * 
     * @OA\Response(
     *     response=200,
     *     description="User deleted successfully",
     *     @Model(type=User::class, groups={"in_out"})
     * )
     *
     * @OA\Response(
     *     response=404,
     *     ref="#/components/responses/404"
     * )
     */
    public function deleteById(User $user): View
    {
        $this->em->remove($user);
        $this->em->flush();

        return $this->view($user);
    }
}
