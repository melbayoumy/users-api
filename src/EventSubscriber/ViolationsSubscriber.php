<?php

namespace App\EventSubscriber;

use FOS\RestBundle\View\View;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Component\HttpFoundation\Response;

class ViolationsSubscriber implements EventSubscriberInterface
{
    public function __construct(private ViewHandlerInterface $viewHandler)
    {
    }

    public function onKernerException(ExceptionEvent $event)
    {
        $ex = $event?->getThrowable();

        if (!$ex instanceof UnprocessableEntityHttpException) {
            return;
        }

        $data = json_decode($ex->getMessage(), true);

        if ($data === null) {
            return;
        }

        $view = View::create($data, $ex->getStatusCode() ?: Response::HTTP_UNPROCESSABLE_ENTITY);

        $response = $this->viewHandler->handle($view, $event->getRequest());

        $event->setResponse($response);
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernerException',
        ];
    }
}
