<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    const USERS_COUNT = 105;

    private Generator $faker;
    private ObjectManager $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->faker = Factory::Create();

        for ($i = 0; $i < self::USERS_COUNT; $i++) {
            $this->createUser();
        }

        $this->manager->flush();
    }

    private function createUser(): void
    {
        $user = new User();
        $user->setEmail($this->faker->email);
        $user->setFirstname($this->faker->firstName);
        $user->setSurname($this->faker->lastName);

        $this->manager->persist($user);
    }
}
