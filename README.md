# USERS-API
Symfony API project to manage user as a resource 

## Requirements
* php 8.0.7
* mysql 5.7

## Tools
* [Symfony 5](https://symfony.com/)
* [Composer](https://getcomposer.org/)
* [FOSRestBundle](https://symfony.com/doc/current/bundles/FOSRestBundle/index.html)
* [NelmioApiDocBundle](https://symfony.com/doc/4.x/bundles/NelmioApiDocBundle/index.html)

## Run locally
### Create database
* Access Mysql on your machine using root user e.g. for windows ``mysql -u<root-user> -p``
* Create database ``CREATE DATABASE `users-api`;``
* Create admin user ``CREATE USER 'users-api' IDENTIFIED BY 'test';``
* Assign privileges to user ``GRANT ALL PRIVILEGES ON `users-api`.* TO `users-api`@`%`;``

### Run project
* Go in the project directory
* In `.env` file, update `DATABASE_URL` according to your database configuration.
* Install dependencies ``php composer.phar install``
* Create database tables ``php ./bin/console doctrine:migrations:migrate``
* Import fake users data ``php ./bin/console doctrine:fixtures:load``
* Spin local server ``php -S 127.0.0.1:8080 -t public``
* Now the api server is spinning on [http://127.0.0.1:8080](http://127.0.0.1:8080). The api is running in **debuggable dev environment**
* You can access Open Api documentation on [http://127.0.0.1:8080/doc/v1](http://127.0.0.1:8080/doc/v1) or on [http://127.0.0.1:8080](http://127.0.0.1:8080) for json format
* *NOTICE:* The api is secured via **Basic Auth** with **in memory** user provider. A test client credentials are as follows:
    * username: test-client
    * password: test

## Way forward
- Introduce **pagination** to `GET` group endpoint
- Enhance **Auth** via introducing `Session Cookie`, `JWT` or `OAuth2`
- Add **throttling** to avoid **Brute-force attacks**
- Adjust **CORS** managment via [Nelmio Cors Bundle](https://github.com/nelmio/NelmioCorsBundle)
- Add **testing** startegy (End-to-End, Integration, Unit)
- Add **logging** functionality 
- Manage prod/dev envirnoments
